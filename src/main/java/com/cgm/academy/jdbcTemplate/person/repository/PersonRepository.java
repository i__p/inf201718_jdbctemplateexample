package com.cgm.academy.jdbcTemplate.person.repository;

import com.cgm.academy.jdbcTemplate.person.model.Person;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PersonRepository {

    Person findById(long id);

    List<Person> findAll();

    int deleteById(long id);

    int insert(Person Person);

    int insertBatch(List<Person> personList);

    int update(Person Person);

    int count();

    void savePersonByPreparedStatement(Person person);

    List<Person> findAllWithResultSet();

    void insertInOneTransaction(List<Person> personList);

    @Transactional(isolation= Isolation.SERIALIZABLE)
    void insertInOneTransactionSerializable(List<Person> personList);
}
