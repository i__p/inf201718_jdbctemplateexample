package com.cgm.academy.jdbcTemplate.person.repository.impl;

import com.cgm.academy.jdbcTemplate.person.model.Person;
import com.cgm.academy.jdbcTemplate.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// repository jest componentem
@Repository
public class PersonJDBCTemplateRepository implements PersonRepository {

    @Autowired // pole oznaczone jako wstrzykiwany bean
    JdbcTemplate jdbcTemplate;

    @Override
    public Person findById(long id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM person WHERE id=?"
                , new Object[]{id}
                , new PersonRowMapper());
    }

    @Override
    public List<Person> findAll() {
        return jdbcTemplate.query("SELECT * FROM Person", new PersonRowMapper());
    }

    @Override
    public int deleteById(long id) {
        return jdbcTemplate.update("DELETE FROM Person WHERE id=?", id);
    }

    @Override
    public int insert(Person person) {
        return jdbcTemplate.update("INSERT INTO Person (id, firstName, lastName, dob) VALUES(?, ?, ?, ?)",
                person.getId(), person.getFirstName(), person.getLastName(), Date.valueOf(person.getDob()));
    }

    @Override
    public int insertBatch(List<Person> personList) {
        String sql = "INSERT INTO Person(Id, FirstName, LastName, DOB) VALUES (?, ?, ?, ?)";
        int[] inserted = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Person person = personList.get(i);
                ps.setLong(1, person.getId());
                ps.setString(2, person.getFirstName());
                ps.setString(3, person.getLastName());
                ps.setDate(4, Date.valueOf(person.getDob()));
            }

            public int getBatchSize() {
                return personList.size();
            }
        });
        return Arrays.stream(inserted).sum();
    }

    @Override
    public int update(Person person) {
        return jdbcTemplate.update("UPDATE Person SET firstName = ?, lastName = ?, DOB = ? WHERE id = ?",
                person.getFirstName(), person.getLastName(), Date.valueOf(person.getDob()), person.getId());
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject("SELECT count(1) FROM Person", Integer.class);
    }

    class PersonRowMapper implements RowMapper<Person> {
        @Override
        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
            Person person = new Person();
            person.fillFieldsFromResultSet(rs);
            return person;
        }
    }

    @Override
    public void savePersonByPreparedStatement(Person person) {
        String query = "INSERT INTO Person VALUES(?,?,?,?)";
        jdbcTemplate.execute(query, (PreparedStatementCallback<Boolean>) ps -> {
            ps.setLong(1, person.getId());
            ps.setString(2, person.getFirstName());
            ps.setString(3, person.getLastName());
            ps.setDate(4, Date.valueOf(person.getDob()));
            return ps.execute();
        });
    }

    @Override
    public List<Person> findAllWithResultSet() {
        return jdbcTemplate.query("SELECT * FROM Person", rs -> {
            List<Person> list = new ArrayList<>();
            while (rs.next()) {
                Person person = new Person();
                person.fillFieldsFromResultSet(rs);
                list.add(person);
            }
            return list;
        });
    }

    @Override
    @Transactional
    public void insertInOneTransaction(List<Person> personList) {
        personList.forEach(this::insert);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void insertInOneTransactionSerializable(List<Person> personList) {
        personList.forEach(this::insert);
    }

}
