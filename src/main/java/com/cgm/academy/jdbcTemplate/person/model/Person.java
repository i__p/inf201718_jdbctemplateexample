package com.cgm.academy.jdbcTemplate.person.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate dob;

    public void fillFieldsFromResultSet(ResultSet rs) throws SQLException {
        this.id = rs.getLong("id");
        this.firstName = rs.getString("firstName");
        this.lastName = rs.getString("lastName");
        Date dob = rs.getDate("DOB");
        if(null!=dob){
            this.dob = dob.toLocalDate();
        }
    }

}
