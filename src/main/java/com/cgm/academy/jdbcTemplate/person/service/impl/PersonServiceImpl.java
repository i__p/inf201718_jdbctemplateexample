package com.cgm.academy.jdbcTemplate.person.service.impl;

import com.cgm.academy.jdbcTemplate.person.exception.PersonHasNoDateOfBirth;
import com.cgm.academy.jdbcTemplate.person.model.Person;
import com.cgm.academy.jdbcTemplate.person.repository.PersonRepository;
import com.cgm.academy.jdbcTemplate.person.service.PersonService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

// service to component z contextu
@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    // wstrzykiwanie przez konstruktor
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Integer howOld(Person p) throws PersonHasNoDateOfBirth {
        if (null == p.getDob()) {
            throw new PersonHasNoDateOfBirth();
        }
        return Math.toIntExact(ChronoUnit.YEARS.between(p.getDob(), LocalDate.now()));
    }

    @Override
    public String findInitialsById(Long id) {
        Person person = findPersonById(id);
        return person.getFirstName().substring(0,1) + person.getLastName().substring(0,1);
    }

    @Override
    public void addPerson(Person p) {
        personRepository.insert(p);
    }

    @Override
    public Person findPersonById(Long id) {
        return personRepository.findById(id);
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public void update(Person person) {
        personRepository.update(person);
    }

    @Override
    public int removePerson(Long id) {
        return personRepository.deleteById(id);
    }

    @Override
    public int addInBatch(List<Person> people) {
        return personRepository.insertBatch(people);
    }

    @Override
    public void addPersonPrepStat(Person person) {
        personRepository.savePersonByPreparedStatement(person);
    }

    @Override
    public int size() {
        return personRepository.count();
    }

    @Override
    public List<Person> findAllWithResultSet() {
        return personRepository.findAllWithResultSet();
    }
}
