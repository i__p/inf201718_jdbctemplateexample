package com.cgm.academy.jdbcTemplate.person.service;

import com.cgm.academy.jdbcTemplate.person.exception.PersonHasNoDateOfBirth;
import com.cgm.academy.jdbcTemplate.person.model.Person;

import java.util.List;

public interface PersonService {

    Integer howOld(Person p) throws PersonHasNoDateOfBirth;
    String findInitialsById(Long id);

    // ponizej tylko crudowe funkcjonalnosci przykrywające repository
    void addPerson(Person p);
    Person findPersonById(Long id);
    List<Person> findAll();
    void update(Person person);
    int removePerson(Long id);
    int addInBatch(List<Person> people);
    void addPersonPrepStat(Person person);
    int size();
    List<Person> findAllWithResultSet();
}
