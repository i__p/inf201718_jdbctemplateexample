package com.cgm.academy.jdbcTemplate;

import com.cgm.academy.jdbcTemplate.person.exception.PersonHasNoDateOfBirth;
import com.cgm.academy.jdbcTemplate.person.model.Person;
import com.cgm.academy.jdbcTemplate.person.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
public class JdbcTemplateApplication implements CommandLineRunner {

    private PersonService personService;

    // wstrzykiwanie przez setter
    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public static void main(String[] args) {
        SpringApplication.run(JdbcTemplateApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("Person with id 1: {}", personService.findPersonById(1L));
        log.info("All from person: {}", personService.findAll());
        personService.addPerson(new Person(3L, "Joe", "Public", LocalDate.of(1980,3,5)));
        personService.update(new Person(1L, "Jane", "Average", LocalDate.of(1976,6,1)));
        int countOfDeletedRows = personService.removePerson(2L);
        log.info(countOfDeletedRows + " rows deleted");
        log.info("All from person: {}", personService.findAll());

        List<Person> people = new ArrayList<>();
        people.add(new Person(4L,"Kate", "Ordinary", LocalDate.of(1991,4,22)));
        people.add(new Person(5L,"Kalle", "Svensson", LocalDate.of(1995,2,14)));
        people.add(new Person(6L,"Jane", "Plain", LocalDate.of(1999,11,23)));
        people.add(new Person(7L,"Seán", "Citizen", LocalDate.of(1993,9,15)));
        int countOfinsertedRows= personService.addInBatch(people);
        log.info(countOfinsertedRows + " rows added");

        log.info("All from person: {}", personService.findAll());

        log.info("Person added using PreparedStatement.");
        personService.addPersonPrepStat(new Person(8L,"Peter", "Smith", LocalDate.of(2001,1,1)));

        log.info("There are: {} person in DB", personService.size());

        List<Person> allWithResultSet = personService.findAllWithResultSet();
        allWithResultSet.stream().map(Person::toString).forEach(log::info);

        log.info("Initials: {}", allWithResultSet.stream()
                                    .map(p -> personService.findInitialsById(p.getId()))
                                    .collect(Collectors.joining(",")));

        allWithResultSet.forEach(p -> {
            try {
                log.info("Person: " + p + ", age:" + personService.howOld(p).toString());
            } catch (PersonHasNoDateOfBirth personHasNoDateOfBirth) {
                log.warn("Person: {} has no DOB", p);
            }
        });

    }
}
