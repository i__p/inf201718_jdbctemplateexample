package com.cgm.academy.jdbcTemplate.person;

import com.cgm.academy.jdbcTemplate.conf.PersonServiceConf;
import com.cgm.academy.jdbcTemplate.person.model.Person;
import com.cgm.academy.jdbcTemplate.person.service.impl.PersonServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes= PersonServiceConf.class, loader=AnnotationConfigContextLoader.class)
public class PersonServiceTest {

    private static Person testedPerson;
    public static void setTestedPerson(Person testedPerson) {
        PersonServiceTest.testedPerson = testedPerson;
    }

    @Autowired
    PersonServiceImpl personService;

    @Test
    @DisplayName("Should find person for given id")
    public void shouldFindPerson() {
        Long id = new Random().nextLong();
        Person personById = personService.findPersonById(testedPerson.getId());
        Assert.assertEquals(testedPerson.getFirstName(), personById.getFirstName());
        Assert.assertEquals(testedPerson.getLastName(), personById.getLastName());
    }
}