package com.cgm.academy.jdbcTemplate.conf;


import com.cgm.academy.jdbcTemplate.person.PersonServiceTest;
import com.cgm.academy.jdbcTemplate.person.model.Person;
import com.cgm.academy.jdbcTemplate.person.repository.PersonRepository;
import com.cgm.academy.jdbcTemplate.person.service.impl.PersonServiceImpl;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;

@Configuration
public class PersonServiceConf {

    @MockBean
    PersonRepository personRepository;

    private static final String PERSON_FNAME = "John";
    private static final String PERSON_LNAME = "Doe";
    private static final Person person = new Person(1L, PERSON_FNAME, PERSON_LNAME, LocalDate.now());

    @Bean
    public PersonServiceImpl personService() {
        Mockito.when(personRepository.findById(any(Long.class))).thenReturn(person);
        return new PersonServiceImpl(personRepository);
    }

    @Bean
    public PersonServiceTest personServiceTest() {
        PersonServiceTest.setTestedPerson(person);
        return new PersonServiceTest();
    }
}